import sys


class GrafanaSyncerError(Exception):
    '''
        General exception with predefined default properties.
    '''
    fatal = True


class HttpResponseStatusNotOkError(GrafanaSyncerError):
    '''
        Exception raised when an unexpected status code is returned from
        an HTTP Server.

        :param status_code: The status code that has been returned by the server.
        :param error_message: The error message that has been returned by the
            server in the response body.
    '''
    def __init__(self,
                 status_code: str | int = 'unspecified',
                 error_message: str = 'unspecified',
                 url: str = 'unspecified',
                 method: str = 'unspecified',
                 fatal: bool = True
    ) -> None:
        self.status_code = status_code
        self.error_message = error_message
        self.url = url
        self.method = method
        self.message = 'Unexpected HTTP response code returned. Status code:' + \
                        f' {status_code}. Message: {error_message}. Method:' + \
                        f' {method}. URL: {url}.'
        self.fatal = fatal

    def __str__(self) -> str:
        return self.message


class ClientConfigurationError(GrafanaSyncerError):
    '''
        Exception raised when a Grafana client has been misconfigured.

        :param message: The explanation message to display when the exception is thrown.
    '''
    def __init__(self, message: str, fatal: bool = True) -> None:
        self.message = message
        self.fatal = fatal

    def __str__(self) -> str:
        return self.message


class ClientNotDefinedError(ClientConfigurationError):
    '''
        Exception raised when a Grafana client has not been
        defined when expected.
    '''
    def __init__(self, client_type: str = 'Grafana client object', fatal: bool = True) -> None:
        self.message = f'{client_type} is expected but not defined.'
        self.fatal = fatal

    def __str__(self) -> str:
        return self.message


class PropertyNotDefinedError(GrafanaSyncerError):
    '''
        Exception raised when a Grafana object property has not
        been defined when expected.
    '''
    def __init__(self, property_name: str = 'Grafana object property', fatal: bool = True) -> None:
        self.message = f'{property_name} is expected but not defined.'
        self.fatal = fatal

    def __str__(self) -> str:
        return self.message


class ConfigError(GrafanaSyncerError):
    '''
        Exception raised when an error related to parsing the
        configuration file has occuered.
    '''
    def __init__(self, message: str, fatal: bool = True) -> None:
        self.fatal = fatal
        self.message = message
        sys.tracebacklimit = 0

    def __str__(self) -> str:
        return self.message


class ObjectUidNotFoundError(GrafanaSyncerError):
    '''
        Exception raised when an object could not be found
        by its UID attribute.
    '''
    def __init__(self, uid: str, fatal: bool = True) -> None:
        self.uid = uid
        self.message = f'Object with UID {self.uid} not found.'
        self.fatal = fatal

    def __str__(self) -> str:
        return self.message


class InvalidUserInputError(GrafanaSyncerError):
    '''
        Exception raised when incorrect user input has been received.
    '''
    pass

