from __future__ import annotations
import tomllib
import os
from pathlib import Path
from argparse import ArgumentParser, Namespace

from pydantic import BaseModel, model_validator

from .clients.http import GrafanaHttpClient
from .clients.fs import GrafanaFsClient
from .clients.all import GrafanaClients
from .exceptions import ConfigError
from .utils import resolve_env_references


class RootValidator(BaseModel):
    '''
        The class that holds the validators that
        should be triggered in all configuration objects.
    '''
    @model_validator(mode='after')
    @classmethod
    def resolve_env_references(cls, values):
        for value in values:
            if isinstance(value[1], str):
                setattr(values, value[0], resolve_env_references(value[1]))
        return values


class ConfigClientsFs(RootValidator):
    '''
        The class represents the configuration of
        the application's Filesystem Client.

        :param data_path: The path to the directory where
            Grafana configuration should be saved to.
    '''
    data_path: str


class ConfigClientsHttp(RootValidator):
    '''
        The class represents the configuration of
        the application's HTTP Client.

        :param url: The URL of Grafana server.
        :param sa_token: Grafana service account authentication token.
        :param ca_cert: The path to Grafana server's custom CA file in PEM format.
        :param default_org_id: Grafana Org ID to perform API calls against.
    '''
    url: str
    sa_token: str
    ca_cert: str | None = None
    default_org_id: str = '1'

    @model_validator(mode='after')
    @classmethod
    def resolve_env_references(cls, values):
        for value in values:
            if isinstance(value[1], str):
                setattr(values, value[0], resolve_env_references(value[1]))
        return values


class ConfigClients(RootValidator):
    '''
        The class is a container for all Client configuration
        objects that are supported by the application.
    '''
    server: ConfigClientsHttp
    filesystem: ConfigClientsFs


class ConfigNamespace(RootValidator):
    '''
        The class represents the configuration for a single
        Namespace.

        :param clients: Application Clients configuration object.
    '''
    clients: ConfigClients

    def __get_configured_client_fields(self) -> list[str]:
        fields = []
        model = self.clients.model_dump()
        for field in model:
            if model[field] is None:
                continue
            fields.append(field)
        return fields

    def populate_parser(self, ns_name: str, parent_parser: ArgumentParser) -> ArgumentParser:
        '''
            Add CLI attributes relevant to the Namepsace object to the parent CLI parser.

            :param ns_name: The name of the namespace.
            :param parent_parser: The parent CLI parser object to append to. 
        '''
        client_fields = self.__get_configured_client_fields()
        parent_parser.description = 'Perform Grafana Syncer operations against ' \
                                    + ns_name.upper() + ' namespace.'
        parent_parser.add_argument('--source',
                          type=str,
                          required=True,
                          choices=client_fields,
                          help='The source to export the configuration from.')
        parent_parser.add_argument('--dest',
                          type=str,
                          required=True,
                          choices=client_fields,
                          help='The destination to import the configuration to.')
        return parent_parser


class Config(RootValidator):
    '''
        The main configuration class.

        :param namespaces: A dictionary of Namespace configuration objects.
    '''
    namespaces: dict[str, ConfigNamespace]

    @classmethod
    def get_config_file_path(cls) -> Path:
        '''
            Retrieve configuration file path based on
            the environment variable or the default value.
        '''
        path = Path(os.getenv('GRAFANA_SYNCER_CONFIG_FILE', default='./config.toml'))
        if not path.exists():
            raise ConfigError(
                f'The configuration file "{path}" does not exist.' + \
                ' Set it via GRAFANA_SYNCER_CONFIG_FILE environment variable.'
            )
        if not path.is_file():
            raise ConfigError(f'"{path}" is not a file.')
        try:
            path.open()
        except PermissionError:
            raise ConfigError(f'The configuration file "{path}" is not readable.')
        return path

    @classmethod
    def populate_from_file(cls) -> Config:
        '''
            Return the initialized instance of the class with the
            values loaded from the application's configuration file.

            :param file_path: The path to the application's configuration file.
        '''
        file_path = cls.get_config_file_path()
        with open(file_path, 'rb') as file:
            config = tomllib.load(file)
        return Config(namespaces=config)

    def process_cli_args(self) -> Namespace:
        '''
            Generate an Argparse.Namespace object that
            holds parsed CLI arguments passed to the application.
        '''
        app_parser = ArgumentParser(
            description='Export and import configuration from and to Grafana server.',
            epilog='Specify the path to the configuration file' \
                    + ' in GRAFANA_SYNCER_CONFIG_FILE environment variable.' \
                    + ' Default: ./config.toml.')
        subparsers = app_parser.add_subparsers(dest='namespace')
        for ns_name in self.namespaces:
            ns_parser = subparsers.add_parser(ns_name)
            self.namespaces[ns_name].populate_parser(ns_name, ns_parser)
        args = app_parser.parse_args()
        if args.source == args.dest:
            raise ConfigError('--source and --dest must be set to different values!')
        return args

    def init_clients(self, ns: str) -> GrafanaClients:
        '''
            Initialize Grafana clients for a single application namepsace.

            :param ns: The namespace to perform operations in.
        '''
        http_client = GrafanaHttpClient(
                        base_url=self.namespaces[ns].clients.server.url,
                        sa_token=self.namespaces[ns].clients.server.sa_token,
                        ca=self.namespaces[ns].clients.server.ca_cert,
                        default_org_id=self.namespaces[ns].clients.server.default_org_id)
        fs_client = GrafanaFsClient(
            data_path=self.namespaces[ns].clients.filesystem.data_path)
        clients = GrafanaClients(http_client, fs_client)
        return clients

