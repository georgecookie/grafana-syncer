from __future__ import annotations
from typing import TYPE_CHECKING, Any, get_args
import json

from .types import GRAFANA_OBJECT_SOURCE
from ..clients.all import GrafanaClients
from ..exceptions import (ClientNotDefinedError,
                          ObjectUidNotFoundError,
                          PropertyNotDefinedError)
# Avoid circular input when using type hints
if TYPE_CHECKING:
    from .folder import GrafanaFolder


class GrafanaAlertRules:
    def __init__(self, alert_rules: list[GrafanaAlertRule]) -> None:
        self._alert_rules = alert_rules

    @property
    def alert_rules(self) -> list[GrafanaAlertRule]:
        return self._alert_rules

    def __repr__(self) -> str:
        return str(self.alert_rules)

    def __getattr__(self, method: Any) -> Any:
        return getattr(self.alert_rules, method)

    def __setitem__(self, cnt: int, item: GrafanaAlertRule) -> None:
        self.alert_rules[cnt] = item

    def __len__(self) -> int:
        return len(self.alert_rules)

    def __getitem__(self, item: int) -> GrafanaAlertRule:
        return self.alert_rules[item]

    def __sub__(self, operand: GrafanaAlertRules) -> GrafanaAlertRules:
        local_uids = [i.uid for i in self.alert_rules]
        operand_uids = [i.uid for i in operand]
        difference_uids = set(local_uids) - set(operand_uids)
        alert_rules = []
        for uid in difference_uids:
            alert_rule = self.get_by_uid(uid)
            alert_rules.append(alert_rule)
        return GrafanaAlertRules(alert_rules)

    def get_by_uid(self, uid: str) -> GrafanaAlertRule:
        '''
            Get Grafana alert rule object by its UID attribute.

            :param uid: UID of Grafana alert rule.
        '''
        for alert_rule in self.alert_rules:
            if alert_rule.uid == uid:
                return alert_rule
        raise ObjectUidNotFoundError(uid)

    def get_subset_by_folder_uid(self, folder_uid: str) -> GrafanaAlertRules:
        '''
            Get a list of Grafana alert rules by a folder UID.

            :param folder_uid: UID of Grafana folder.
        '''
        alert_rules = []
        for alert_rule in self.alert_rules:
            if alert_rule.folder.uid == folder_uid:
                alert_rules.append(alert_rule)
        return GrafanaAlertRules(alert_rules)


class GrafanaAlertRule:
    '''
        The class holds methods for interacting with Grafana Dashboard
        objects.
    '''
    def __init__(self,
                 uid: str,
                 source: GRAFANA_OBJECT_SOURCE,
                 folder: GrafanaFolder,
                 clients: GrafanaClients
    ) -> None:
        self.uid = uid
        self.set_source(source)
        self._folder = folder
        self._clients = clients
        self._path_suffix = f'{self.folder.path_suffix}/{self.folder.uid}/alerts'
        # Values to populate
        self.schema: dict | None = None

    def __repr__(self) -> str:
        return str(self.as_repr_values())

    @property
    def folder(self) -> GrafanaFolder:
        return self._folder

    @property
    def clients(self) -> GrafanaClients:
        return self._clients

    @property
    def path_suffix(self) -> str:
        return self._path_suffix

    def set_source(self, source: GRAFANA_OBJECT_SOURCE) -> None:
        '''
            Set the source where Grafana object has been loaded from.

            :param source: The source name.
        '''
        source_options = get_args(GRAFANA_OBJECT_SOURCE)
        if source not in source_options:
            raise ValueError()
        self._source = source

    def as_repr_values(self) -> dict:
        ''' Return the values that represent the object accross all interfaces. '''
        return {
            '__object_type__': 'dashboard',
            '__source__': self._source,
            'uid': self.uid,
            'schema': self.schema
        }

    def populate_from_server(self) -> None:
        '''
            Populate the unknown values of the object from
            the data stored on Grafana server.
        '''
        if self.clients.http_client is None:
            raise ClientNotDefinedError(client_type='http_client')
        alert_rule_resp_text = self.clients.http_client.get(
                                    url_suffix=f'/api/v1/provisioning/alert-rules/{self.uid}',
                                    expected_status_codes=[200])
        alert_rule_resp = json.loads(alert_rule_resp_text)
        self.schema = alert_rule_resp
        self.set_source('server')

    def populate_from_filesystem(self) -> None:
        '''
            Populate the unknown values of the object from
            the data stored on the filesystem.
        '''
        if self.clients.fs_client is None:
            raise ClientNotDefinedError(client_type='fs_client')
        properties = self.clients.fs_client.\
                        read_object(name=self.uid,
                                    path_suffix=self.path_suffix)
        self.schema = properties['schema']
        self.set_source('filesystem')

    def populate_from_source(self) -> None:
        '''
            Populate the unknown values of the object from
            the data stored on the object's source storage.
        '''
        if self._source == 'server':
            self.populate_from_server()
        if self._source == 'filesystem':
            self.populate_from_filesystem()

    def create_on_sever(self) -> None:
        '''
            Create Grafana object on Grafana server.
        '''
        if self.clients.http_client is None:
            raise ClientNotDefinedError(client_type='http_client')
        if self.schema is None:
            raise PropertyNotDefinedError(property_name='schema')
        self.clients.http_client.post(url_suffix=f'/api/v1/provisioning/alert-rules',
                                      headers={"X-Disable-Provenance": "true"},
                                      body=self.schema,
                                      expected_status_codes=[201])

    def update_on_server(self) -> None:
        '''
            Update Grafana object that is stored on Grafana server.
        '''
        if self.clients.http_client is None:
            raise ClientNotDefinedError(client_type='http_client')
        if self.schema is None:
            raise PropertyNotDefinedError(property_name='schema')
        self.clients.http_client.put(url_suffix=f'/api/v1/provisioning/alert-rules/{self.uid}',
                                     headers={"X-Disable-Provenance": "true"},
                                     body=self.schema,
                                     expected_status_codes=[200])

    def save_to_filesystem(self) -> None:
        '''
            Save the Grafana dashboard object representation to the local filesystem.
        '''
        if self.clients.fs_client is None:
            raise ClientNotDefinedError(client_type='fs_client')
        self.clients.fs_client.\
            write_object(name=self.uid,
                         properties=self.as_repr_values(),
                         path_suffix=self.path_suffix)

    def remove_from_server(self) -> None:
        '''
            Remove Grafana dashboard object from Grafana server.
        '''
        if self.clients.http_client is None:
            raise ClientNotDefinedError(client_type='http_client')
        self.clients.http_client.delete(url_suffix=f'/api/v1/provisioning/alert-rules/{self.uid}',
                                        headers={"X-Disable-Provenance": "true"},
                                        expected_status_codes=[204])

    def remove_from_filesystem(self) -> None:
        '''
            Remove Grafana dashboard object representation from the filesystem.
        '''
        if self.clients.fs_client is None:
            raise ClientNotDefinedError(client_type='fs_client')
        self.clients.fs_client.delete_object(self.uid, self.path_suffix)

    def remove_from_source(self) -> None:
        '''
            Remove Grafana object from its original source.
        '''
        if self._source == 'server':
            self.remove_from_server()
        if self._source == 'filesystem':
            self.remove_from_filesystem()

