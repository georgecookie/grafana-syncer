from __future__ import annotations
from typing import TYPE_CHECKING, Any, get_args
import json

from .types import GRAFANA_OBJECT_SOURCE
from ..utils import get_os_user
from ..clients.all import GrafanaClients
from ..exceptions import (ClientNotDefinedError,
                          ObjectUidNotFoundError,
                          HttpResponseStatusNotOkError)
# Avoid circular input when using type hints
if TYPE_CHECKING:
    from .folder import GrafanaFolder


class GrafanaDashboards:
    def __init__(self, dashboards: list[GrafanaDashboard]) -> None:
        self._dashboards = dashboards

    @property
    def dashboards(self) -> list[GrafanaDashboard]:
        return self._dashboards

    def __repr__(self) -> str:
        return str(self.dashboards)

    def __getattr__(self, method: Any) -> Any:
        return getattr(self.dashboards, method)

    def __setitem__(self, cnt: int, item: GrafanaDashboard) -> None:
        self.dashboards[cnt] = item

    def __len__(self) -> int:
        return len(self.dashboards)

    def __getitem__(self, item: int) -> GrafanaDashboard:
        return self.dashboards[item]

    def __sub__(self, operand: GrafanaDashboards) -> GrafanaDashboards:
        local_uids = [i.uid for i in self.dashboards]
        operand_uids = [i.uid for i in operand]
        difference_uids = set(local_uids) - set(operand_uids)
        dashboards = []
        for uid in difference_uids:
            dashboard = self.get_by_uid(uid)
            dashboards.append(dashboard)
        return GrafanaDashboards(dashboards)

    def get_by_uid(self, uid: str) -> GrafanaDashboard:
        '''
            Get Grafana dashboard object by its UID attribute.

            :param uid: UID of Grafana dashboard.
        '''
        for dashboard in self.dashboards:
            if dashboard.uid == uid:
                return dashboard
        raise ObjectUidNotFoundError(uid)


class GrafanaDashboard:
    '''
        The class holds methods for interacting with Grafana Dashboard
        objects.
    '''
    def __init__(self,
                 uid: str,
                 source: GRAFANA_OBJECT_SOURCE,
                 folder: GrafanaFolder,
                 clients: GrafanaClients
    ) -> None:
        self.uid = uid
        self.set_source(source)
        self._folder = folder
        self._clients = clients
        self._path_suffix = f'{self.folder.path_suffix}/{self.folder.uid}/dashboards'
        # Values to populate
        self.meta: dict | None = None
        self.dashboard: dict | None = None
        self.permissions: list | None = None

    def __repr__(self) -> str:
        return str(self.as_repr_values())

    @property
    def folder(self) -> GrafanaFolder:
        return self._folder

    @property
    def clients(self) -> GrafanaClients:
        return self._clients

    @property
    def path_suffix(self) -> str:
        return self._path_suffix

    def set_source(self, source: GRAFANA_OBJECT_SOURCE) -> None:
        '''
            Set the source where Grafana object has been loaded from.

            :param source: The source name.
        '''
        source_options = get_args(GRAFANA_OBJECT_SOURCE)
        if source not in source_options:
            raise ValueError()
        self._source = source

    def as_repr_values(self) -> dict:
        ''' Return the values that represent the object accross all interfaces. '''
        return {
            '__object_type__': 'dashboard',
            '__source__': self._source,
            'uid': self.uid,
            'meta': self.meta,
            'dashboard': self.dashboard,
            'permissions': self.permissions,
        }

    def populate_from_server(self) -> None:
        '''
            Populate the unknown values of the object from
            the data stored on Grafana server.
        '''
        if self.clients.http_client is None:
            raise ClientNotDefinedError(client_type='http_client')
        dashboards_resp_text = self.clients.http_client.get(
                                    url_suffix=f'/api/dashboards/uid/{self.uid}',
                                    expected_status_codes=[200])
        permissions_resp_text = self.clients.http_client.get(
                                    url_suffix=f'/api/dashboards/uid/{self.uid}/permissions',
                                    expected_status_codes=[200])
        dashboards_resp = json.loads(dashboards_resp_text)
        permissions_resp = json.loads(permissions_resp_text)
        self.meta = dashboards_resp['meta']
        self.dashboard = dashboards_resp['dashboard']
        self.permissions = permissions_resp
        self.set_source('server')

    def populate_from_filesystem(self) -> None:
        '''
            Populate the unknown values of the object from
            the data stored on the filesystem.
        '''
        if self.clients.fs_client is None:
            raise ClientNotDefinedError(client_type='fs_client')
        properties = self.clients.fs_client.\
                        read_object(name=self.uid,
                                    path_suffix=self.path_suffix)
        self.meta = properties['meta']
        self.dashboard = properties['dashboard']
        self.permissions = properties['permissions']
        self.set_source('filesystem')

    def populate_from_source(self) -> None:
        '''
            Populate the unknown values of the object from
            the data stored on the object's source storage.
        '''
        if self._source == 'server':
            self.populate_from_server()
        if self._source == 'filesystem':
            self.populate_from_filesystem()

    def create_on_sever(self) -> None:
        '''
            Create Grafana object on Grafana server.
        '''
        if self.clients.http_client is None:
            raise ClientNotDefinedError(client_type='http_client')
        payload = {
            "folderUid": self.folder.uid,
            "dashboard": self.dashboard,
            "message": f'{get_os_user('UNSPECIFIED USER')} - automated sync',
            "overwrite": True
        }
        payload['dashboard']['id'] = None
        self.clients.http_client.post(url_suffix=f'/api/dashboards/db',
                                      body=payload,
                                      expected_status_codes=[200])
        payload = {"items": self.permissions}
        self.clients.http_client.post(url_suffix=f'/api/dashboards/uid/{self.uid}/permissions',
                                      body=payload,
                                      expected_status_codes=[200])

    def update_on_server(self) -> None:
        '''
            Update Grafana object that is stored on Grafana server.
        '''
        if self.clients.http_client is None:
            raise ClientNotDefinedError(client_type='http_client')
        payload = {
            "folderUid": self.folder.uid,
            "dashboard": self.dashboard,
            "message": f'{get_os_user('UNSPECIFIED USER')} - automated sync',
            "overwrite": True
        }
        try:
            self.clients.http_client.post(url_suffix=f'/api/dashboards/db',
                                          body=payload,
                                          expected_status_codes=[200])
        except HttpResponseStatusNotOkError as http_error:
            if http_error.status_code == 404:
                self.create_on_sever()
            else:
                raise http_error
        payload = {"items": self.permissions}
        self.clients.http_client.post(url_suffix=f'/api/dashboards/uid/{self.uid}/permissions',
                                      body=payload,
                                      expected_status_codes=[200])

    def save_to_filesystem(self) -> None:
        '''
            Save the Grafana dashboard object representation to the local filesystem.
        '''
        if self.clients.fs_client is None:
            raise ClientNotDefinedError(client_type='fs_client')
        self.clients.fs_client.\
            write_object(name=self.uid,
                         properties=self.as_repr_values(),
                         path_suffix=self.path_suffix)

    def remove_from_server(self) -> None:
        '''
            Remove Grafana dashboard object from Grafana server.
        '''
        if self.clients.http_client is None:
            raise ClientNotDefinedError(client_type='http_client')
        self.clients.http_client.delete(url_suffix=f'/api/dashboards/uid/{self.uid}',
                                        expected_status_codes=[200])

    def remove_from_filesystem(self) -> None:
        '''
            Remove Grafana dashboard object representation from the filesystem.
        '''
        if self.clients.fs_client is None:
            raise ClientNotDefinedError(client_type='fs_client')
        self.clients.fs_client.delete_object(self.uid, self.path_suffix)

    def remove_from_source(self) -> None:
        '''
            Remove Grafana object from its original source.
        '''
        if self._source == 'server':
            self.remove_from_server()
        if self._source == 'filesystem':
            self.remove_from_filesystem()

