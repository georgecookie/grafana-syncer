from __future__ import annotations
from typing import Any, get_args
import json

from .dashboard import GrafanaDashboard, GrafanaDashboards
from .library import GrafanaLibraryElement, GrafanaLibraryElements
from .alerts import GrafanaAlertRule, GrafanaAlertRules
from .types import GRAFANA_OBJECT_SOURCE
from ..clients.all import GrafanaClients
from ..exceptions import (ClientNotDefinedError,
                          PropertyNotDefinedError,
                          ObjectUidNotFoundError)


class GrafanaFolders:
    def __init__(self, folders: list[GrafanaFolder]) -> None:
        self._folders = folders

    @property
    def folders(self) -> list[GrafanaFolder]:
        return self._folders

    def __repr__(self) -> str:
        return str(self.folders)

    def __getattr__(self, method: Any) -> Any:
        return getattr(self.folders, method)

    def __setitem__(self, cnt: int, item: GrafanaFolder) -> None:
        self.folders[cnt] = item

    def __len__(self) -> int:
        return len(self.folders)

    def __getitem__(self, item: int) -> GrafanaFolder:
        return self.folders[item]

    def __sub__(self, operand: GrafanaFolders) -> GrafanaFolders:
        local_uids = [i.uid for i in self._folders]
        operand_uids = [i.uid for i in operand]
        difference_uids = set(local_uids) - set(operand_uids)
        folders = []
        for uid in difference_uids:
            folder = self.get_by_uid(uid)
            folders.append(folder)
        return GrafanaFolders(folders)

    def get_by_uid(self, uid: str) -> GrafanaFolder:
        '''
            Get Grafana folder object by its UID attribute.

            :param uid: UID of Grafana folder.
        '''
        for folder in self.folders:
            if folder.uid == uid:
                return folder
        raise ObjectUidNotFoundError(uid)


class GrafanaFolder:
    '''
        The class holds methods for interacting with Grafana Folder
        objects.

        :param uid: Grafana object UID.
        :param clients: Grafana Clients configuration object.
        :param source: Object source identifier.
    '''
    def __init__(self,
                 uid: str,
                 clients: GrafanaClients,
                 source: GRAFANA_OBJECT_SOURCE
    ) -> None:
        self.uid = uid
        self._clients = clients
        self.set_source(source)
        self._path_suffix = 'folders'
        # Values to populate
        self.title: str | None = None
        self.id_: str | None = None
        self.permissions: list | None = None

    def __repr__(self) -> str:
        return str(self.as_repr_values())

    @property
    def clients(self) -> GrafanaClients:
        return self._clients

    @property
    def path_suffix(self) -> str:
        return self._path_suffix

    @classmethod
    def get_all_folders_from_server(cls, clients: GrafanaClients) -> GrafanaFolders:
        '''
            Get all folders from Grafana server.

            :param clients: Grafana clients object to make requests with.
        '''
        if clients.http_client is None:
            raise ClientNotDefinedError(client_type='http_client')
        response_text = clients.http_client.get(url_suffix='/api/folders', expected_status_codes=[200])
        response = json.loads(response_text)
        folders = []
        for folder_data in response:
            folder = GrafanaFolder(uid=folder_data['uid'], clients=clients, source='server')
            folders.append(folder)
        return GrafanaFolders(folders)

    @classmethod
    def get_all_folders_from_filesystem(cls, clients: GrafanaClients) -> GrafanaFolders:
        '''
            Get all Grafana folder object from the filesystem.

            :param clients: Grafana clients object to make requests with.
        '''
        if clients.fs_client is None:
            raise ClientNotDefinedError(client_type='fs_client')
        folder_uids = clients.fs_client.list_object_names('folders')
        folders = []
        for folder_uid in folder_uids:
            folder = GrafanaFolder(uid=folder_uid, clients=clients, source='filesystem')
            folders.append(folder)
        return GrafanaFolders(folders)

    def set_source(self, source: GRAFANA_OBJECT_SOURCE) -> None:
        '''
            Set the source where Grafana object has been loaded from.

            :param source: The source name.
        '''
        source_options = get_args(GRAFANA_OBJECT_SOURCE)
        if source not in source_options:
            raise ValueError()
        self._source = source

    def as_repr_values(self) -> dict:
        ''' Return the values that represent the object accross all interfaces. '''
        return {
            '__object_type__': 'folder',
            '__source__': self._source,
            'uid': self.uid,
            'id': self.id_,
            'title': self.title,
            'permissions': self.permissions
        }

    def populate_from_server(self) -> None:
        '''
            Populate the unknown values of the object from
            the data stored on Grafana server.
        '''
        if self.clients.http_client is None:
            raise ClientNotDefinedError(client_type='http_client')
        folders_resp_text = self.clients.http_client.get(
                                    url_suffix=f'/api/folders/{self.uid}',
                                    expected_status_codes=[200])
        permissions_resp_text = self.clients.http_client.get(
                                    url_suffix=f'/api/folders/{self.uid}/permissions',
                                    expected_status_codes=[200])
        folders_resp = json.loads(folders_resp_text)
        permissions_resp = json.loads(permissions_resp_text)
        self.title = folders_resp['title']
        self.id_ = folders_resp['id']
        self.permissions = permissions_resp
        self.set_source('server')

    def populate_from_filesystem(self) -> None:
        '''
            Populate the unknown values of the object from
            the data stored on the filesystem.
        '''
        if self.clients.fs_client is None:
            raise ClientNotDefinedError(client_type='fs_client')
        properties = self.clients.fs_client.\
                        read_object(name=self.uid,
                                    path_suffix=self.path_suffix)
        self.title = properties['title']
        self.id_ = properties['id']
        self.permissions = properties['permissions']
        self.set_source('filesystem')

    def populate_from_source(self) -> None:
        '''
            Populate the unknown values of the object from
            the data stored on the object's source storage.
        '''
        if self._source == 'server':
            self.populate_from_server()
        if self._source == 'filesystem':
            self.populate_from_filesystem()

    def create_on_sever(self) -> None:
        '''
            Create Grafana object on Grafana server.
        '''
        if self.clients.http_client is None:
            raise ClientNotDefinedError(client_type='http_client')
        payload = {
            "uid": self.uid,
            "title": self.title
        }
        self.clients.http_client.post(url_suffix=f'/api/folders',
                                      body=payload,
                                      expected_status_codes=[200])
        payload = {"items": self.permissions}
        self.clients.http_client.post(url_suffix=f'/api/folders/{self.uid}/permissions',
                                      body=payload,
                                      expected_status_codes=[200])

    def update_on_server(self) -> None:
        '''
            Update Grafana object that is stored on Grafana server.
        '''
        if self.clients.http_client is None:
            raise ClientNotDefinedError(client_type='http_client')
        payload = {
            "title": self.title,
            "overwrite": True
        }
        self.clients.http_client.put(url_suffix=f'/api/folders/{self.uid}',
                                      body=payload,
                                      expected_status_codes=[200])
        payload = {"items": self.permissions}
        self.clients.http_client.post(url_suffix=f'/api/folders/{self.uid}/permissions',
                                      body=payload,
                                      expected_status_codes=[200])

    def save_to_filesystem(self) -> None:
        '''
            Save the Grafana folder object representation to the local filesystem.
        '''
        if self.clients.fs_client is None:
            raise ClientNotDefinedError(client_type='fs_client')
        self.clients.fs_client.write_object(name=f'{self.uid}',
                                            properties=self.as_repr_values(),
                                            path_suffix=self.path_suffix)

    def remove_from_server(self) -> None:
        '''
            Remove Grafana folder object and all of its children
            from the server.
        '''
        if self.clients.http_client is None:
            raise ClientNotDefinedError(client_type='http_client')
        # Can't delete a folder if it contains alert rules or library elements.
        library_elements = self.get_library_elements_from_source()
        if len(library_elements) > 0:
            for dashboard in self.get_dashboards_from_server():
                dashboard.remove_from_server()
            for lib_elem in library_elements:
                lib_elem.remove_from_server()
        for alert_rule in self.get_alert_rules_from_source():
            alert_rule.remove_from_server()
        self.clients.http_client.delete(url_suffix=f'/api/folders/{self.uid}',
                                        expected_status_codes=[200])

    def remove_from_filesystem(self) -> None:
        '''
            Remove Grafana folder object and all of its children
            from the filesystem.
        '''
        if self.clients.fs_client is None:
            raise ClientNotDefinedError(client_type='fs_client')
        self.clients.fs_client.delete_object(self.uid, self.path_suffix)

    def remove_from_source(self) -> None:
        '''
            Remove Grafana object from its original source.
        '''
        if self._source == 'server':
            self.remove_from_server()
        if self._source == 'filesystem':
            self.remove_from_filesystem()

    def get_dashboards_from_server(self) -> GrafanaDashboards:
        '''
            Get the list of Grafana dashboards that belong to the folder.
        '''
        if self.clients.http_client is None:
            raise ClientNotDefinedError(client_type='http_client')
        if self.id_ is None:
            raise PropertyNotDefinedError('id')
        response_text = self.clients.http_client.get(url_suffix=f'/api/search?folderIds={self.id_}',
                                                     expected_status_codes=[200])
        response = json.loads(response_text)
        dashboards = []
        for dashboard_data in response:
            dashboard = GrafanaDashboard(uid=dashboard_data['uid'],
                                         source='server',
                                         folder=self,
                                         clients=self.clients)
            dashboard.populate_from_server()
            dashboards.append(dashboard)
        return GrafanaDashboards(dashboards)

    def get_dashboards_from_filesystem(self) -> GrafanaDashboards:
        '''
            Get a list of Grafana dashboard objects sourced from the filesystem.
        '''
        dashboards = []
        if self.clients.fs_client is None:
            raise ClientNotDefinedError(client_type='fs_client')
        dashboard_uids = self.clients.fs_client.\
            list_object_names(path_suffix=f'folders/{self.uid}/dashboards')
        for uid in dashboard_uids:
            dashboard = GrafanaDashboard(uid=uid,
                                         source='filesystem',
                                         folder=self,
                                         clients=self.clients)
            dashboard.populate_from_filesystem()
            dashboards.append(dashboard)
        return GrafanaDashboards(dashboards)

    def get_dashboards_from_source(self) -> GrafanaDashboards:
        '''
            Get a list of Grafana dashboard objects
            loaded from the folder's source.
        '''
        dashboards = GrafanaDashboards([])
        if self._source == 'server':
            dashboards = self.get_dashboards_from_server()
        if self._source == 'filesystem':
            dashboards = self.get_dashboards_from_filesystem()
        return dashboards

    def get_library_elements_from_server(self) -> GrafanaLibraryElements:
        '''
            Get the list of Grafana librart elements that belong to the folder.
        '''
        if self.clients.http_client is None:
            raise ClientNotDefinedError(client_type='http_client')
        if self.id_ is None:
            raise PropertyNotDefinedError('id')
        response_text = self.clients.http_client.get(
                            url_suffix=f'/api/library-elements?folderFilter={self.id_}&perPage=10000',
                            expected_status_codes=[200])
        response = json.loads(response_text)
        lib_elems = []
        for lib_elem_data in response['result']['elements']:
            lib_elem = GrafanaLibraryElement(uid=lib_elem_data['uid'],
                                             source='server',
                                             folder=self,
                                             clients=self.clients)
            lib_elem.populate_from_server()
            lib_elems.append(lib_elem)
        return GrafanaLibraryElements(lib_elems)

    def get_library_elements_from_filesystem(self) -> GrafanaLibraryElements:
        '''
            Get a list of Grafana alert rules
            objects sourced from the filesystem.
        '''
        lib_elems = []
        if self.clients.fs_client is None:
            raise ClientNotDefinedError(client_type='fs_client')
        lib_elem_uids = self.clients.fs_client.\
            list_object_names(path_suffix=f'{self.path_suffix}/{self.uid}/library')
        for uid in lib_elem_uids:
            lib_elem = GrafanaLibraryElement(uid=uid,
                                             source='filesystem',
                                             folder=self,
                                             clients=self.clients)
            lib_elem.populate_from_filesystem()
            lib_elems.append(lib_elem)
        return GrafanaLibraryElements(lib_elems)

    def get_library_elements_from_source(self) -> GrafanaLibraryElements:
        '''
            Get a list of Grafana library element objects
            loaded from the folder's source.
        '''
        lib_elems = GrafanaLibraryElements([])
        if self._source == 'server':
            lib_elems = self.get_library_elements_from_server()
        if self._source == 'filesystem':
            lib_elems = self.get_library_elements_from_filesystem()
        return lib_elems

    def get_alert_rules_from_server(self) -> GrafanaAlertRules:
        '''
            Get all Grafana alert rules.

            :param folders: The list of all folders to bind the retreived alert rules with.
        '''
        if self.clients.http_client is None:
            raise ClientNotDefinedError(client_type='http_client')
        response_text = self.clients.http_client.get(
                            url_suffix=f'/api/v1/provisioning/alert-rules',
                            expected_status_codes=[200])
        response = json.loads(response_text)
        alert_rules = []
        for alert_rules_data in response:
            if alert_rules_data['folderUID'] == self.uid:
                alert_rule = GrafanaAlertRule(
                                uid=alert_rules_data['uid'],
                                source='server',
                                folder=self,
                                clients=self.clients)
                alert_rule.populate_from_server()
                alert_rules.append(alert_rule)
        return GrafanaAlertRules(alert_rules)

    def get_alert_rules_from_filesystem(self) -> GrafanaAlertRules:
        '''
            Get a list of Grafana alert rules
            objects sourced from the filesystem.
        '''
        alert_rules = []
        if self.clients.fs_client is None:
            raise ClientNotDefinedError(client_type='fs_client')
        alert_rules_uids = self.clients.fs_client.\
            list_object_names(path_suffix=f'{self.path_suffix}/{self.uid}/alerts')
        for uid in alert_rules_uids:
            alert_rule = GrafanaAlertRule(uid=uid,
                                          source='filesystem',
                                          folder=self,
                                          clients=self.clients)
            alert_rule.populate_from_filesystem()
            alert_rules.append(alert_rule)
        return GrafanaAlertRules(alert_rules)

    def get_alert_rules_from_source(self) -> GrafanaAlertRules:
        '''
            Get a list of all Grafana alert rules.

            :param folders: The list of all folders to bind the retreived alert rules with.
        '''
        alert_rules = GrafanaAlertRules([])
        if self._source == 'server':
            alert_rules = self.get_alert_rules_from_server()
        if self._source == 'filesystem':
            alert_rules = self.get_alert_rules_from_filesystem()
        return alert_rules

