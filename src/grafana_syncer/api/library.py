from __future__ import annotations
from typing import TYPE_CHECKING, Any, get_args
import json

from .types import GRAFANA_OBJECT_SOURCE
from ..clients.all import GrafanaClients
from ..exceptions import (ClientNotDefinedError,
                          ObjectUidNotFoundError)
# Avoid circular input when using type hints
if TYPE_CHECKING:
    from .folder import GrafanaFolder


class GrafanaLibraryElements:
    def __init__(self, library_elements: list[GrafanaLibraryElement]) -> None:
        self._library_elements = library_elements

    @property
    def library_elements(self) -> list[GrafanaLibraryElement]:
        return self._library_elements

    def __repr__(self) -> str:
        return str(self.library_elements)

    def __getattr__(self, method: Any) -> Any:
        return getattr(self.library_elements, method)

    def __setitem__(self, cnt: int, item: GrafanaLibraryElement) -> None:
        self.library_elements[cnt] = item

    def __len__(self) -> int:
        return len(self.library_elements)

    def __getitem__(self, item: int) -> GrafanaLibraryElement:
        return self.library_elements[item]

    def __sub__(self, operand: GrafanaLibraryElements) -> GrafanaLibraryElements:
        local_uids = [i.uid for i in self.library_elements]
        operand_uids = [i.uid for i in operand]
        difference_uids = set(local_uids) - set(operand_uids)
        lib_elems = []
        for uid in difference_uids:
            lib_elem= self.get_by_uid(uid)
            lib_elems.append(lib_elem)
        return GrafanaLibraryElements(lib_elems)

    def get_by_uid(self, uid: str) -> GrafanaLibraryElement:
        '''
            Get Grafana library object by its UID attribute.

            :param uid: UID of Grafana library.
        '''
        for lib_elem in self.library_elements:
            if lib_elem.uid == uid:
                return lib_elem
        raise ObjectUidNotFoundError(uid)


class GrafanaLibraryElement:
    '''
        The class holds methods for interacting with Grafana Library Panel
        objects.
    '''
    def __init__(self,
                 uid: str,
                 source: GRAFANA_OBJECT_SOURCE,
                 folder: GrafanaFolder,
                 clients: GrafanaClients
    ) -> None:
        self.uid = uid
        self.set_source(source)
        self._folder = folder
        self._clients = clients
        self._path_suffix = f'{self.folder.path_suffix}/{self.folder.uid}/library'
        # Values to populate
        self.name: str | None = None
        self.kind: int | None = None
        self.model: dict | None = None
        self.version: int | None = None

    def __repr__(self) -> str:
        return str(self.as_repr_values())

    @property
    def folder(self) -> GrafanaFolder:
        return self._folder

    @property
    def clients(self) -> GrafanaClients:
        return self._clients

    @property
    def path_suffix(self) -> str:
        return self._path_suffix

    def set_source(self, source: GRAFANA_OBJECT_SOURCE) -> None:
        '''
            Set the source where Grafana object has been loaded from.

            :param source: The source name.
        '''
        source_options = get_args(GRAFANA_OBJECT_SOURCE)
        if source not in source_options:
            raise ValueError()
        self._source = source

    def as_repr_values(self) -> dict:
        ''' Return the values that represent the object accross all interfaces. '''
        return {
            '__object_type__': 'library-element',
            '__source__': self._source,
            'uid': self.uid,
            'name': self.name,
            'kind': self.kind,
            'model': self.model
        }

    def populate_from_server(self) -> None:
        '''
            Populate the unknown values of the object from
            the data stored on Grafana server.
        '''
        if self.clients.http_client is None:
            raise ClientNotDefinedError(client_type='http_client')
        panels_resp_text = self.clients.http_client.get(
                                    url_suffix=f'/api/library-elements/{self.uid}',
                                    expected_status_codes=[200])
        panels_resp = json.loads(panels_resp_text)
        self.name = panels_resp['result']['name']
        self.kind = panels_resp['result']['kind']
        self.model = panels_resp['result']['model']
        self.version = int(panels_resp['result']['version'])
        self.set_source('server')

    def populate_from_filesystem(self) -> None:
        '''
            Populate the unknown values of the object from
            the data stored on the filesystem.
        '''
        if self.clients.fs_client is None:
            raise ClientNotDefinedError(client_type='fs_client')
        properties = self.clients.fs_client.\
                        read_object(name=self.uid,
                                    path_suffix=self.path_suffix)
        self.name = properties['name']
        self.kind = properties['kind']
        self.model = properties['model']
        self.set_source('filesystem')

    def populate_from_source(self) -> None:
        '''
            Populate the unknown values of the object from
            the data stored on the object's source storage.
        '''
        if self._source == 'server':
            self.populate_from_server()
        if self._source == 'filesystem':
            self.populate_from_filesystem()

    def create_on_sever(self) -> None:
        '''
            Create Grafana object on Grafana server.
        '''
        if self.clients.http_client is None:
            raise ClientNotDefinedError(client_type='http_client')
        payload = {
            "uid": self.uid,
            "folderUid": self.folder.uid,
            "name": self.name,
            "model": self.model,
            "kind": self.kind
        }
        self.clients.http_client.post(url_suffix=f'/api/library-elements',
                                      body=payload,
                                      expected_status_codes=[200])

    def update_on_server(self) -> None:
        '''
            Update Grafana object that is stored on Grafana server.
        '''
        if self.clients.http_client is None:
            raise ClientNotDefinedError(client_type='http_client')
        if self.version is None:
            panels_resp_text = self.clients.http_client.get(
                                    url_suffix=f'/api/library-elements/{self.uid}',
                                    expected_status_codes=[200])
            panels_resp = json.loads(panels_resp_text)
            self.version = int(panels_resp['result']['version'])
        payload = {
            "uid": self.uid,
            "folderUid": self.folder.uid,
            "name": self.name,
            "model": self.model,
            "kind": self.kind,
            "version": self.version
        }
        self.clients.http_client.patch(url_suffix=f'/api/library-elements/{self.uid}',
                                       body=payload,
                                       expected_status_codes=[200])

    def save_to_filesystem(self) -> None:
        '''
            Save Grafana library element object representation to the local filesystem.
        '''
        if self.clients.fs_client is None:
            raise ClientNotDefinedError(client_type='fs_client')
        self.clients.fs_client.\
            write_object(name=self.uid,
                         properties=self.as_repr_values(),
                         path_suffix=self.path_suffix)

    def remove_from_server(self) -> None:
        '''
            Remove Grafana library element object from Grafana server.
        '''
        if self.clients.http_client is None:
            raise ClientNotDefinedError(client_type='http_client')
        self.clients.http_client.delete(url_suffix=f'/api/library-elements/{self.uid}',
                                        expected_status_codes=[200])

    def remove_from_filesystem(self) -> None:
        '''
            Remove Grafana library element object representation from the filesystem.
        '''
        if self.clients.fs_client is None:
            raise ClientNotDefinedError(client_type='fs_client')
        self.clients.fs_client.delete_object(self.uid, self.path_suffix)

    def remove_from_source(self) -> None:
        '''
            Remove Grafana object from its original source.
        '''
        if self._source == 'server':
            self.remove_from_server()
        if self._source == 'filesystem':
            self.remove_from_filesystem()

