from __future__ import annotations

import requests

from ..exceptions import HttpResponseStatusNotOkError


class GrafanaHttpClient:
    '''
        Grafana HTTP Client configuration class.

        :param base_url: The URL of Grafana server.
        :param sa_token: The service account token to authenticate
            to Garfana server with.
        :param ca: The path to Grafana server CA certificate in PEM format.
            If set to False, TLS verification will be skipped.
        :param default_org_id: Grafana Org ID to perform API calls against.
    '''
    def __init__(self,
                 base_url: str,
                 sa_token: str,
                 ca: str | bool | None = None,
                 default_org_id: str | None = None
    ) -> None:
        self.base_url = base_url
        self.sa_token = sa_token
        self.ca = ca
        self.base_headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {sa_token}'
        }
        if default_org_id is not None:
            self.base_headers['X-Grafana-Org-Id'] = default_org_id

    def __check_response_code(self,
                              status_code: int,
                              expected_status_codes: list[int],
                              error_message: str = 'unspecified',
                              url: str = 'unspecified',
                              method: str = 'unspecified'
    ) -> None:
        status_ok = False
        for status in expected_status_codes:
            if status == status_code:
                status_ok = True
        if not status_ok:
            raise HttpResponseStatusNotOkError(
                status_code=status_code, error_message=error_message, url=url, method=method)

    def get(self,
            url_suffix: str,
            headers: dict = {},
            expected_status_codes: list[int] | None = None,
    ) -> str:
        '''
            Send a GET request to Grafana server. Returns response text.

            :param url_suffix: URL suffix that will be appended to the base server URL.
            :param headers: Additional headers that will be appended to the request.
            :param expected_status_codes: A list of expected response status codes.
                An exception will be returned if none match the response.
        '''
        response = requests.get(
            url=f'{self.base_url}{url_suffix}',
            headers=self.base_headers | headers,
            verify=self.ca
        )
        if expected_status_codes is not None:
            self.__check_response_code(
                response.status_code,
                expected_status_codes,
                response.text,
                response.url,
                "GET"
            )
        return response.text

    def post(self,
             url_suffix: str,
             headers: dict = {},
             body: dict = {},
             expected_status_codes: list[int] | None = None
    ) -> str:
        '''
            Send a POST request to Grafana server. Returns response text.

            :param url_suffix: URL suffix that will be appended to the base server URL.
            :param headers: Additional headers that will be appended to the request.
            :param body: The body of the request that will be serialized to JSON and sent to the server.
            :param expected_status_codes: A list of expected response status codes.
                An exception will be returned if none match the response.
        '''
        response = requests.post(   
            url=f'{self.base_url}{url_suffix}',
            headers=self.base_headers | headers,
            json=body,
            verify=self.ca
        )
        if expected_status_codes is not None:
            self.__check_response_code(
                response.status_code,
                expected_status_codes,
                response.text,
                response.url,
                "POST"
            )
        return response.text

    def put(self,
            url_suffix: str,
            headers: dict = {},
            body: dict = {},
            expected_status_codes: list[int] | None = None,
    ) -> str:
        '''
            Send a PUT request to Grafana server. Returns response text.

            :param url_suffix: URL suffix that will be appended to the base server URL.
            :param headers: Additional headers that will be appended to the request.
            :param body: The body of the request that will be serialized to JSON and sent to the server.
            :param expected_status_codes: A list of expected response status codes.
                An exception will be returned if none match the response.
        '''
        response = requests.put(   
            url=f'{self.base_url}{url_suffix}',
            headers=self.base_headers | headers,
            json=body,
            verify=self.ca
        )
        if expected_status_codes is not None:
            self.__check_response_code(
                response.status_code,
                expected_status_codes,
                response.text,
                response.url,
                "PUT"
            )
        return response.text

    def patch(self,
              url_suffix: str,
              headers: dict = {},
              body: dict = {},
              expected_status_codes: list[int] | None = None,
    ) -> str:
        '''
            Send a PATCH request to Grafana server. Returns response text.

            :param url_suffix: URL suffix that will be appended to the base server URL.
            :param headers: Additional headers that will be appended to the request.
            :param body: The body of the request that will be serialized to JSON and sent to the server.
            :param expected_status_codes: A list of expected response status codes.
                An exception will be returned if none match the response.
        '''
        response = requests.patch(   
            url=f'{self.base_url}{url_suffix}',
            headers=self.base_headers | headers,
            json=body,
            verify=self.ca
        )
        if expected_status_codes is not None:
            self.__check_response_code(
                response.status_code,
                expected_status_codes,
                response.text,
                response.url,
                "PATCH"
            )
        return response.text

    def delete(self,
               url_suffix: str,
               headers: dict = {},
               expected_status_codes: list[int] | None = None,
    ) -> str:
        '''
            Send a DELETE request to Grafana server. Returns response text.

            :param url_suffix: URL suffix that will be appended to the base server URL.
            :param headers: Additional headers that will be appended to the request.
            :param expected_status_codes: A list of expected response status codes.
                An exception will be returned if none match the response.
        '''
        response = requests.delete(   
            url=f'{self.base_url}{url_suffix}',
            headers=self.base_headers | headers,
            verify=self.ca
        )
        if expected_status_codes is not None:
            self.__check_response_code(
                response.status_code,
                expected_status_codes,
                response.text,
                response.url,
                "DELETE"
            )
        return response.text

