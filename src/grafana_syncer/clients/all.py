from .http import GrafanaHttpClient
from .fs import GrafanaFsClient


class GrafanaClients:
    '''
        The class is a container for all supported Grafana Client objects.
    '''
    def __init__(self,
                 http_client: GrafanaHttpClient | None = None,
                 fs_client: GrafanaFsClient | None = None
    ) -> None:
        self._http_client = http_client
        self._fs_client = fs_client

    @property
    def http_client(self) -> GrafanaHttpClient | None:
        return self._http_client

    @property
    def fs_client(self) -> GrafanaFsClient | None:
        return self._fs_client

