from __future__ import annotations
from pathlib import Path
from shutil import rmtree
import json


class GrafanaFsClient:
    ''' Grafana Filesystem Client configuration class. '''
    def __init__(self, data_path: str) -> None:
        self.data_path = Path(data_path)
        self.properties_file = 'properties.json'

    def _is_grafana_object(self, path: Path | str) -> bool:
        '''
            Check if the provided path is a correct Grafana object
            representation on the local filesystem.

            :param path: The Path object that is to be validated.
        '''
        if isinstance(path, str):
            path = Path(path)
        prop_file_path = path / self.properties_file
        return prop_file_path.exists and prop_file_path.is_file()

    def write_object(self, name: str, properties: dict, path_suffix: str = '') -> None:
        '''
            Save a Grafana object (folder, dashboard, etc) to the local filesystem.
            The object is represented as a directory with the object's that holds
            a properties file with the object's properties serialized to JSON format.

            :param name: The name of Grafana object.
            :param properties: The properties of Grafana object.
            :param path_suffix: The path suffix appended to the data path on the
                local filesystem where Grafana object should be stored.
        '''
        properties_serialized = json.dumps(properties, indent=2)
        object_path = self.data_path / path_suffix / name
        object_path.mkdir(parents=True, exist_ok=True)
        with open(object_path / self.properties_file, 'w') as file:
            file.write(properties_serialized)

    def read_object(self, name: str, path_suffix: str = '') -> dict:
        '''
            Load a Grafana object (folder, dashboard, etc) the memory from the local
            filesystem.

            :param name: The name of the object.
            :param path_suffix: The path suffix appended to the data path on the
                local filesystem where Grafana object should be searched at.
        '''
        object_properties_file = self.data_path / path_suffix / name / self.properties_file
        with open(object_properties_file, 'r') as file:
            return json.loads(file.read())

    def delete_object(self, name: str, path_suffix: str = '') -> None:
        '''
            Delete Grafana object from the filesystem.

            :param name: The name of Grafana object.
            :param path_suffix: The path suffix appended to the data path on the
                local filesystem where Grafana object should be searched at.
        '''
        path = self.data_path / path_suffix / name
        if not self._is_grafana_object(path):
            raise Exception(f'Object with name {name} at {path} could not be found.')
        rmtree(path)

    def list_object_names(self, path_suffix: str = '') -> list[str]:
        '''
            Get the list of all Grafana object names found
            on the local filesystem.

            :param path_suffix: The path suffix appended to the data path on the
                local filesystem where Grafana objects should be searched at.
        '''
        objects = []
        paths = (self.data_path / path_suffix).glob('*')
        for path in list(paths):
            if not self._is_grafana_object(path):
                continue
            objects.append(path.name)
        return objects

