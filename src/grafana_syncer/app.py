from typing import TypeVar
import logging

from .config import Config
from .clients.all import GrafanaClients
from .api.folder import GrafanaFolder, GrafanaFolders
from .api.dashboard import GrafanaDashboards
from .api.library import GrafanaLibraryElements
from .api.alerts import GrafanaAlertRules
from .exceptions import (InvalidUserInputError,
                         ObjectUidNotFoundError,
                         HttpResponseStatusNotOkError)


T = TypeVar("T",
            GrafanaFolders,
            GrafanaDashboards,
            GrafanaLibraryElements,
            GrafanaAlertRules)


def sync_delete(source_objects: T, dest_objects: T) -> T:
    '''
        Delete the resources on the destination storage
        that are not present on the source storage.

        :param source_objects: All Grafana objects from the source storage.
        :param dest_objects: All Grafana objects from the destination storage.
    '''
    objects_to_delete = dest_objects - source_objects
    for obj in objects_to_delete:
        try:
            obj.remove_from_source()
        except HttpResponseStatusNotOkError as e:
            logging.warning(f'Could not sync object with UID {obj.uid}. {e}')
            continue
    return objects_to_delete


def sync_create(dest: str, source_objects: T, dest_objects: T) -> None:
    '''
        Create Grafana resources from the source storage to the
        destination storage.

        :param dest: The destination storage type.
        :param source_objects: All Grafana objects from the source storage.
        :param dest_objects: All Grafana objects from the destination storage.
    '''
    objects = source_objects - dest_objects
    for obj in objects:
        try:
            if dest == 'server':
                obj.create_on_sever()
            if dest == 'filesystem':
                obj.save_to_filesystem()
        except HttpResponseStatusNotOkError as e:
            logging.warning(f'Could not sync object with UID {obj.uid}. {e}')
            continue


def sync_update(dest: str, source_objects: T, dest_objects: T) -> None:
    '''
        Update Grafana resources from the source storage to the
        destination storage.

        :param dest: The destination storage type.
        :param source_objects: All Grafana objects from the source storage.
        :param dest_objects: All Grafana objects from the destination storage.
    '''
    objects = source_objects - (source_objects - dest_objects)
    for obj in objects:
        try:
            if dest == 'server':
                obj.update_on_server()
            if dest == 'filesystem':
                obj.save_to_filesystem()
        except HttpResponseStatusNotOkError as e:
            logging.warning(f'Could not sync object with UID {obj.uid}. {e}')
            continue


def list_folders(source: str, clients: GrafanaClients) -> GrafanaFolders:
    '''
        Get a list of grafana resources present at the passed source.

        :param source: The source to pull the configuration from.
        :param clients: Grafana Client configuration objects.
    '''
    if source == 'server':
        folders = GrafanaFolder.get_all_folders_from_server(clients)
    elif source == 'filesystem':
        folders = GrafanaFolder.get_all_folders_from_filesystem(clients)
    else:
        raise InvalidUserInputError(f'Invalid destination specified: {source}.')
    for folder in folders:
        try:
            folder.populate_from_source()
        except HttpResponseStatusNotOkError as e:
            logging.warning(f'Could not sync folder with UID {folder.uid}. {e}')
            continue
    return folders


def sync_folders(
    source: str,
    dest: str,
    clients: GrafanaClients
) -> tuple[GrafanaFolders, GrafanaFolders]:
    '''
        Synchronize Grafana folders between source and destination
        storages. Returns (source_folders, dest_folders).

        :param source: The source storage type.
        :param dest: The destination storage type.
        :param clients: Grafana Client configuration objects.
    '''
    source_folders = list_folders(source=source, clients=clients)
    dest_folders = list_folders(source=dest, clients=clients)
    sync_delete(source_folders, dest_folders)
    sync_create(dest, source_folders, dest_folders)
    sync_update(dest, source_folders, dest_folders)
    return source_folders, dest_folders


def get_dashboards_from_folders(
    source_folder: GrafanaFolder,
    dest_folders: GrafanaFolders
) -> tuple[GrafanaDashboards, GrafanaDashboards]:
    '''
        Get Grafana source and destination dashboards from
        Grafana folder objects. Returns the tuple of
        (source_dashboards, dest_dashboards).

        :param source_folder: Grafana folder object from source storage.
        :param dest_folders: The list of all Grafana folders from the destination storage.
    '''
    source_dashboards = source_folder.get_dashboards_from_source()
    try:
        dest_dashboards = dest_folders.get_by_uid(source_folder.uid) \
                                            .get_dashboards_from_source()
    except ObjectUidNotFoundError:
        dest_dashboards = GrafanaDashboards([])
    return source_dashboards, dest_dashboards


def get_library_elements_from_folders(
    source_folder: GrafanaFolder,
    dest_folders: GrafanaFolders
) -> tuple[GrafanaLibraryElements, GrafanaLibraryElements]:
    '''
        Get Grafana source and destination library elements from
        Grafana folder objects. Returns the tuple of
        (source_lib_elems, dest_lib_elems).

        :param source_folder: Grafana folder object from source storage.
        :param dest_folders: The list of all Grafana folders from the
            destination storage.
    '''
    source_lib_elems = source_folder.get_library_elements_from_source()
    try:
        dest_lib_elems = dest_folders.get_by_uid(source_folder.uid) \
                                            .get_library_elements_from_source()
    except ObjectUidNotFoundError:
        dest_lib_elems = GrafanaLibraryElements([])
    return source_lib_elems, dest_lib_elems


def get_alert_rules_from_folders(
    source_folder: GrafanaFolder,
    dest_folders: GrafanaFolders
) -> tuple[GrafanaAlertRules, GrafanaAlertRules]:
    '''
        Get Grafana source and destination alert rules from
        Grafana folder objects. Returns the tuple of
        (source_alert_rules, dest_alert_rules).

        :param source_folder: Grafana folder object from source storage.
        :param dest_folders: The list of all Grafana folders from the
            destination storage.
    '''
    source_alert_rules = source_folder.get_alert_rules_from_source()
    try:
        dest_alert_rules = dest_folders.get_by_uid(source_folder.uid) \
                                            .get_alert_rules_from_source()
    except ObjectUidNotFoundError:
        dest_alert_rules = GrafanaAlertRules([])
    return source_alert_rules, dest_alert_rules


def sync_folder_elements(
    dest: str,
    source_folders: GrafanaFolders,
    dest_folders: GrafanaFolders
) -> None:
    '''
        Synchronize all elements that belong to the passed Grafana folders
        between source and destination storages.

        :param dest: The destination storage type.
        :param source_folders: Grafana folders from the source storage.
        :param dest_folders: Grafana folders from the destination storage.
    '''
    for folder in source_folders:
            source_db, dest_db = get_dashboards_from_folders(folder, dest_folders)
            source_lib_elems, dest_lib_elems = get_library_elements_from_folders(
                                                    folder, dest_folders)
            source_alert_rules, dest_alert_rules = get_alert_rules_from_folders(
                                                        folder, dest_folders)
            # Create Library Elements BEFORE Dashboards!
            # Delete Dashboards BEFORE Library elements!
            sync_create(dest, source_lib_elems, dest_lib_elems)
            sync_create(dest, source_db, dest_db)
            sync_update(dest, source_lib_elems, dest_lib_elems)
            sync_update(dest, source_db, dest_db)
            sync_delete(source_db, dest_db)
            sync_delete(source_lib_elems, dest_lib_elems)

            sync_create(dest, source_alert_rules, dest_alert_rules)
            sync_update(dest, source_alert_rules, dest_alert_rules)
            sync_delete(source_alert_rules, dest_alert_rules)


def run() -> None:
    ''' The application's entrypoint. '''
    config = Config.populate_from_file()
    args = config.process_cli_args()
    clients = config.init_clients(args.namespace)

    source_folders, dest_folders = sync_folders(args.source, args.dest, clients)
    sync_folder_elements(args.dest, source_folders, dest_folders)

