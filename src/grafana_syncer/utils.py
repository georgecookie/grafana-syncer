import os
import re


def resolve_env_references(string: str) -> str:
    '''
        Resolve references to evironment variables
        passed within a string.

        :param string: The string that needs to be interpolated.
    '''
    regex = r'\$\{([^}]+)\}'
    match_groups = re.findall(regex, string)
    resolved_string = string
    for match_group in match_groups:
        env_value = os.getenv(match_group)
        resolved_string = re.sub(pattern=regex,
                           repl=str(env_value),
                           string=resolved_string,
                           count=1)
    return resolved_string


def get_os_user(fallback: str) -> str:
    '''
        Return OS system user name that the program runs under.

        :param default: The value to return in case the user cannot be retreived.
    '''
    username_env = os.getenv('USER', os.getenv('USERNAME'))
    if username_env is None:
        return fallback
    return username_env

