Grafana Syncer
=================

## Descritpion
Save and restore Grafana configuration to/from the local filesystem.

## Example

Save the configuration (dashboards, folders, alerts, etc.) from `grafana1`
instance to the filesystem.

```sh
grafana-syncer grafana1 --source server --dest filesystem
```

Upload/restore the configuration from the filesystem to `grafana2` instance.

```sh
grafana-syncer grafana2 --source filesystem --dest server
```

## Installation
1. Make sure **Python 3.12** is installed on the device.
2. Create Python virtual environment.
```sh
python3.12 -m venv .venv
```
3. Activate the environment
```sh
source .venv/bin/activate
```
4. Install the project
```sh
pip install .
```

## Configuration
In order to work, the application requires a properly set
configuration file in **TOML** format. The path to the file can be specified
via `GRAFANA_SYNCER_CONFIG_FILE` environment variable. By default
the path is set to `config.toml` in the current directory.

#### Configuration Options
|Name|Type|Required|Default|Description|
|----|----|----|----|----|
|**namespace**|string|Yes|-|-|The application namespace (ex. tool, prod ...). Can be any text value specified multiple times.|
|**namespace.clients.server.url**|string|Yes|-|The url of Grafana server (ex. http://grafana.example.com).|
|**namespace.clients.server.sa_token**|string|Yes|-|Grafana service account token. The SA must have admin permissions.|
|**namespace.clients.server.ca_cert**|string|No|-|The path to the custom TLS CA file.|
|**namesapce.clients.server.default_org_id**|string|No|'1'|Grafana Org ID to execute queries against.|
|**namespace.clients.filesystem.data_path**|string|Yes||The path on the local filesystem where Grafana configuration should be saved.|

#### Environment Varialbes Interpolation
The configuration processor supports environment variables interpolation
in string data types. The environment variables in the configuration file
must be wrapped in `${}` symbols. For example, in the scenario below,
the option `prod.clients.server.sa_token` will be set to the value of
`GRAFANA_SA_TOKEN` environment variable.
```toml
[prod]
  clients.server.sa_token = ${GRAFANA_SA_TOKEN}
```

## Usage
Each **namespace** specified in the configuration file will be parsed and
added as a seperate sub-command to the application. See command help
for usage instructions.
```sh
grafana-syncer --help
```


## Development
Install addtional dependencies required for development under **dev** namespace
Optionally, install the application in *editable* (option `-e`) mode. This will
allow to edit the source code without having to re-install the application.
```sh
pip install -e '.[dev]'
```

#### Tests
Run `pytest` in the project root directory.

