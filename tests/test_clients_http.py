from grafana_syncer.clients.http import GrafanaHttpClient


def test_GrafanaHttpClient_init():
    base_url = 'https://grafana.example.com'
    sa_token = 'glsa_S3cureTok_en'
    ca = '/tmp/server_ca.pem'
    default_org_id = '2'
    client = GrafanaHttpClient(base_url=base_url,
                               sa_token=sa_token,
                               ca=ca,
                               default_org_id=default_org_id)
    assert client.base_url == base_url
    assert client.sa_token == sa_token
    assert client.ca == ca
    assert client.base_headers['X-Grafana-Org-Id'] == default_org_id

