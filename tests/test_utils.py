import os

from grafana_syncer.utils import resolve_env_references, get_os_user


def test_resolve_env_references():
    os.environ['TEST_VAR'] = 'test_value'
    var = '${TEST_VAR}'
    value = 'test_value'
    append = '_string_appendix_'
    assert resolve_env_references(f'{var}{append}') == f'{value}{append}'
    assert resolve_env_references(f'{append}{var}') == f'{append}{value}'
    assert resolve_env_references(f'{var}{var}') == f'{value}{value}'
    assert resolve_env_references(f'{append}{var}{append}') == f'{append}{value}{append}'
    assert resolve_env_references(f'{var}{append}{var}{append}') == f'{value}{append}{value}{append}'


def test_get_os_user():
    username_env = os.getenv('USER', os.getenv('USERNAME'))
    assert get_os_user(fallback='test') == username_env

