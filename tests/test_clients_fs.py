import pytest
import json
from pathlib import Path

from grafana_syncer.clients.fs import GrafanaFsClient


@pytest.fixture
def example_grafana_folder_data() -> dict:
    properties = {
        "__object_type__": "folder",
        "__source__": "server",
        "uid": "MXyq1QvSz",
        "id": 57,
        "title": "Apps",
        "permissions": []
    }
    return properties


@pytest.fixture
def example_grafana_folder_file(tmp_path, example_grafana_folder_data) -> Path:
    file_path = Path(f"{tmp_path}/{example_grafana_folder_data['uid']}/properties.json")
    json_folder_data = json.dumps(example_grafana_folder_data)
    file_path.parent.mkdir(parents=True)
    file_path.write_text(json_folder_data)
    return file_path


def test_GrafanaFsClient_init(tmp_path):
    client = GrafanaFsClient(data_path=tmp_path)
    assert client.data_path == tmp_path


def test_GrafanaFsClient_read_object(example_grafana_folder_file: Path,
                                     example_grafana_folder_data: dict):
    client = GrafanaFsClient(data_path=str(example_grafana_folder_file.parent.parent))
    data = client.read_object(name=str(example_grafana_folder_file.parent.name))
    assert data == example_grafana_folder_data
    

def test_GrafanaFsClient_write_object(example_grafana_folder_file: Path,
                                      example_grafana_folder_data: dict):
    example_grafana_folder_file.unlink()
    client = GrafanaFsClient(data_path=str(example_grafana_folder_file.parent.parent))
    client.write_object(name=example_grafana_folder_data['uid'],
                        properties=example_grafana_folder_data)
    assert json.loads(example_grafana_folder_file.read_text()) == example_grafana_folder_data


def test_GrafanaFsClient_delete_object(example_grafana_folder_file: Path):
    client = GrafanaFsClient(data_path=str(example_grafana_folder_file.parent.parent))
    client.delete_object(name=example_grafana_folder_file.parent.name)
    assert not example_grafana_folder_file.exists()


def test_GrafanaFsClient_list_object_names(example_grafana_folder_file: Path):
    client = GrafanaFsClient(data_path=str(example_grafana_folder_file.parent.parent))
    names = client.list_object_names()
    assert isinstance(names, list)
    assert names[0] == example_grafana_folder_file.parent.name

