from grafana_syncer.clients.all import GrafanaClients
from grafana_syncer.clients.http import GrafanaHttpClient
from grafana_syncer.clients.fs import GrafanaFsClient


def test_GrafanaClients_init():
    base_url = 'https://grafana.example.com'
    sa_token = 'glsa_S3cureTok_en'
    ca = '/tmp/server_ca.pem'
    default_org_id = '2'
    data_path = '/tmp/data'
    http_client = GrafanaHttpClient(base_url=base_url,
                                    sa_token=sa_token,
                                    ca=ca,
                                    default_org_id=default_org_id)
    fs_client = GrafanaFsClient(data_path=data_path)
    clients = GrafanaClients()
    assert clients.http_client is None
    assert clients.fs_client is None
    clients = GrafanaClients(http_client=http_client, fs_client=fs_client)
    assert isinstance(clients.http_client, GrafanaHttpClient)
    assert clients.http_client == http_client
    assert isinstance(clients.fs_client, GrafanaFsClient)
    assert clients.fs_client == fs_client

