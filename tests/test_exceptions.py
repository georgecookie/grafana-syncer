import pytest

from grafana_syncer.exceptions import (GrafanaSyncerError,
                                       HttpResponseStatusNotOkError,
                                       ClientConfigurationError,
                                       ClientNotDefinedError,
                                       PropertyNotDefinedError,
                                       ConfigError,
                                       ObjectUidNotFoundError,
                                       InvalidUserInputError)


def test_GrafanaSyncerError():
    with pytest.raises(GrafanaSyncerError) as e:
        raise GrafanaSyncerError('Test message')
    assert isinstance(e.value, Exception)
    assert e.value.fatal == True
    assert str(e.value) == 'Test message'


def test_HttpResponseStatusNotOkError():
    status_code=500
    error_message='Test error message'
    url='http://example.com'
    method='POST'
    fatal = False
    with pytest.raises(HttpResponseStatusNotOkError) as e:
        raise HttpResponseStatusNotOkError(status_code=status_code,
                                           error_message=error_message,
                                           url=url,
                                           method=method,
                                           fatal=fatal)
    exception_message = str(e.value)
    assert isinstance(e.value, Exception)
    assert isinstance(e.value, GrafanaSyncerError)
    assert e.value.fatal == fatal
    assert status_code == e.value.status_code
    assert str(status_code) in exception_message
    assert error_message == e.value.error_message
    assert error_message in exception_message
    assert url == e.value.url
    assert url in exception_message
    assert method == e.value.method
    assert method in exception_message


def test_ClientConfigurationError():
    message = 'Test error message'
    fatal = False
    with pytest.raises(ClientConfigurationError) as e:
        raise ClientConfigurationError(message=message,
                                       fatal=fatal)
    exception_message = str(e.value)
    assert isinstance(e.value, Exception)
    assert isinstance(e.value, GrafanaSyncerError)
    assert e.value.fatal == fatal
    assert message == e.value.message
    assert message in exception_message


def test_ClientNotDefinedError():
    client_type = 'http_client'
    fatal = False
    with pytest.raises(ClientNotDefinedError) as e:
        raise ClientNotDefinedError(client_type=client_type,
                                    fatal=fatal)
    exception_message = str(e.value)
    assert isinstance(e.value, Exception)
    assert isinstance(e.value, GrafanaSyncerError)
    assert isinstance(e.value, ClientConfigurationError)
    assert e.value.fatal == fatal
    assert client_type in exception_message


def test_PropertyNotDefinedError():
    property_name = 'clients'
    fatal = False
    with pytest.raises(PropertyNotDefinedError) as e:
        raise PropertyNotDefinedError(property_name=property_name, fatal=fatal)
    exception_message = str(e.value)
    assert isinstance(e.value, Exception)
    assert isinstance(e.value, GrafanaSyncerError)
    assert e.value.fatal == fatal
    assert property_name in exception_message


def test_ConfigError():
    message = 'Test error message'
    fatal = False
    with pytest.raises(ConfigError) as e:
        raise ConfigError(message=message, fatal=fatal)
    exception_message = str(e.value)
    assert isinstance(e.value, Exception)
    assert isinstance(e.value, GrafanaSyncerError)
    assert e.value.fatal == fatal
    assert message == e.value.message
    assert message in exception_message


def test_ObjectUidNotFoundError():
    uid='f5f24db1-4af8-4df5-9f1g-af1d6c8e95d6'
    fatal = False
    with pytest.raises(ObjectUidNotFoundError) as e:
        raise ObjectUidNotFoundError(uid=uid, fatal=fatal)
    exception_message = str(e.value)
    assert isinstance(e.value, Exception)
    assert isinstance(e.value, GrafanaSyncerError)
    assert e.value.fatal == fatal
    assert uid == e.value.uid
    assert uid in exception_message


def test_InvalidUserInputError():
    error_message = 'Test error message'
    with pytest.raises(InvalidUserInputError) as e:
        raise InvalidUserInputError(error_message)
    exception_message = str(e.value)
    assert isinstance(e.value, Exception)
    assert isinstance(e.value, GrafanaSyncerError)
    assert e.value.fatal == True
    assert error_message == exception_message

