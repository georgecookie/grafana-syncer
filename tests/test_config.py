import os
import tomllib
from typing import Any
from pathlib import Path

import pytest
from pydantic import BaseModel

from grafana_syncer.config import (Config, ConfigNamespace, ConfigClients,
                                   ConfigClientsHttp, ConfigClientsFs, RootValidator)
from grafana_syncer.clients.all import GrafanaClients
from grafana_syncer.clients.fs import GrafanaFsClient
from grafana_syncer.clients.http import GrafanaHttpClient
from grafana_syncer.exceptions import ConfigError


@pytest.fixture
def example_config_str() -> str:
    config_str = """
[dev]
  clients.server.url = 'https://grafana.dev.example.com'
  clients.server.sa_token = 'glsa_S3cureTok_en'
  clients.server.ca_cert = '/tmp/dev_server_ca.pem'
  clients.server.default_org_id = '1'
  clients.filesystem.data_path = '/tmp/data/dev'

[prod]
  clients.server.url = 'https://grafana.dev.example.com'
  clients.server.sa_token = 'glsa_SecUr3toKe_n1'
  clients.server.ca_cert = '/tmp/dev_server_ca.pem'
  clients.server.default_org_id = '2'
  # Filesystem Client
  clients.filesystem.data_path = '/tmp/data/dev'
"""
    return config_str


@pytest.fixture
def example_config(example_config_str) -> dict['str', Any]:
    config = tomllib.loads(example_config_str)
    return config


@pytest.fixture
def example_config_file(tmp_path, example_config_str) -> Path:
    config_file_path = Path(f'{tmp_path}/config.toml')
    with open(config_file_path, 'w') as f:
        f.write(example_config_str)
    return config_file_path


def test_Config_init(example_config):
    namespace = 'dev'
    config = Config(namespaces=example_config)
    assert isinstance(config, RootValidator)
    assert isinstance(config.namespaces, dict)
    assert config.namespaces.keys() == example_config.keys()
    assert isinstance(config.namespaces[namespace], ConfigNamespace)


def test_Config_get_config_file_path(example_config_file):
    os.environ['GRAFANA_SYNCER_CONFIG_FILE'] = str(example_config_file)
    config_path = Config.get_config_file_path()
    assert isinstance(config_path, Path)
    assert str(example_config_file) == str(config_path)
    os.environ['GRAFANA_SYNCER_CONFIG_FILE'] = 'Noneexistantpath'
    with pytest.raises(ConfigError):
        Config.get_config_file_path()


def test_Config_populate_from_file(example_config_file, example_config):
    os.environ['GRAFANA_SYNCER_CONFIG_FILE'] = str(example_config_file)
    config = Config.populate_from_file()
    assert isinstance(config, Config)
    assert config.namespaces.keys() == example_config.keys()


def test_Config_init_clients(example_config_file, example_config):
    namespace = 'dev'
    os.environ['GRAFANA_SYNCER_CONFIG_FILE'] = str(example_config_file)
    config = Config.populate_from_file()
    clients = config.init_clients(namespace)
    assert isinstance(clients, GrafanaClients)
    assert isinstance(clients.http_client, GrafanaHttpClient)
    assert clients.http_client.base_url == example_config[namespace]['clients']['server']['url']
    assert clients.http_client.sa_token == example_config[namespace]['clients']['server']['sa_token']
    assert clients.http_client.ca == example_config[namespace]['clients']['server']['ca_cert']
    assert clients.http_client.base_headers['X-Grafana-Org-Id'] == example_config[namespace]['clients']['server']['default_org_id']
    assert isinstance(clients.fs_client, GrafanaFsClient)
    assert clients.fs_client.data_path == Path(example_config[namespace]['clients']['filesystem']['data_path'])


def test_ConfigNamespace_init(example_config):
    namespace = 'dev'
    config_namespace = ConfigNamespace(clients=example_config[namespace]['clients'])
    assert isinstance(config_namespace, RootValidator)
    assert isinstance(config_namespace.clients, ConfigClients)


def test_ConfigClients_init(example_config):
    namespace = 'dev'
    config_clients = ConfigClients(
        server=example_config[namespace]['clients']['server'],
        filesystem=example_config[namespace]['clients']['filesystem']
    )
    assert isinstance(config_clients, RootValidator)
    assert isinstance(config_clients.server, ConfigClientsHttp)
    assert isinstance(config_clients.filesystem, ConfigClientsFs)


def test_ConfigClientsHttp_init(example_config):
    namespace = 'dev'
    config_client_http = ConfigClientsHttp(
        url=example_config[namespace]['clients']['server']['url'],
        sa_token=example_config[namespace]['clients']['server']['sa_token'],
        ca_cert=example_config[namespace]['clients']['server']['ca_cert'],
        default_org_id=example_config[namespace]['clients']['server']['default_org_id']
    )
    assert isinstance(config_client_http, RootValidator)
    assert isinstance(config_client_http.url, str)
    assert config_client_http.url == example_config[namespace]['clients']['server']['url']
    assert isinstance(config_client_http.sa_token, str)
    assert config_client_http.sa_token == example_config[namespace]['clients']['server']['sa_token']
    assert isinstance(config_client_http.ca_cert, str)
    assert config_client_http.ca_cert == example_config[namespace]['clients']['server']['ca_cert']
    assert isinstance(config_client_http.default_org_id, str)
    assert config_client_http.default_org_id == example_config[namespace]['clients']['server']['default_org_id']


def test_ConfigClientsFs_init(example_config):
    namespace = 'dev'
    config_client_fs = ConfigClientsFs(
        data_path=example_config[namespace]['clients']['filesystem']['data_path']
    )
    assert isinstance(config_client_fs, RootValidator)
    assert isinstance(config_client_fs.data_path, str)
    assert config_client_fs.data_path == example_config[namespace]['clients']['filesystem']['data_path']


def test_RootValidator_init():
    assert isinstance(RootValidator(), BaseModel)


def test_RootValidator_resolve_env_references():
    os.environ['TEST_VAR'] = 'TEST_VALUE'
    config_client_fs = ConfigClientsFs(data_path='${TEST_VAR}')
    assert config_client_fs.data_path == 'TEST_VALUE'

